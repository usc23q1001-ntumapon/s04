#python has several structures to store collections
#or multiple items in a single variable

# lists
# lists are similar to arrays in JavaScript in a sense that
#they can contain a collection of data
#To create a list, the square bracket is used([]) is used

names  = ["John", "Paul","George","Ringo"]
programs = ['developer career', 'pi-shape','short courses']
durations = [260, 180, 20]
truth_values = [True,False,True,True,False]

# A list can contain elemsents of different data types
sample_list = ["Apple", 3, False, "Potato", 4, True]

print(sample_list)

#getting the list size
print(len(programs))

#accessing values 
#List can be accessed by providing the index number of the
#element

#access the first item in the list 
print(programs[0])#developer career

#access the last item in the list
print(programs[-1])#short courses

#access the 2nd item in the list
print(durations[1])

#access the whole list
print(durations) # [260, 180, 20]

#Access a range of values
#list [start index:  end index]
print(programs[0:2])
#Note that the index is not included.
#['developer career', 'pi-shape']

#Updating lists
#print the current value 
print(f'Current Value: {programs[2]}')

#Update the value
programs[2] = 'Short Courses'

#print the new value
print(f'New Value: {programs[2]}')

#mini-exercise
#1. create a list of names of 5 students
#2. create a list of grades for the 5 students
#3. use a loop to iterate through the lists printing
# the following format:
#the grade of John is 100

students = ["John", "Carl","Thomas","Bryle", "JC"]
grades = [100,99,98,97,96]

i = 0
while i < len(students):
	print(f"The grade of {students[i]} is {grades[i]}.")
	i += 1

#List manipulation
#List has methods that can be used to manipulate the 
#elements within 

#adding list items - append() method allows to insert 
#items to a list

programs.append('global')
print(programs)

#deleting list items - the "del" keyword can be used to 
#delete elements in the list
#add net iten to the duration list
durations.append(360)
print(durations)

#delete the last item on the list
del durations[-1]
print(durations)

#Membership checks - the "in" Keyword checks if the element
#is in the list
#returns true or false 
print(20 in durations)
print(500 in durations)

#sorting lists - the sort() method sorsts the lists alpha
#numerically, ascending by default
print(names)
names.sort()
print(names)

#emptying the list - the clear() method is used to empty the 
#contents of the list

test_list = [1,3,5,7,9]
print(test_list)
test_list.clear()
print(test_list)

#Dictionaries are used to store data values in our key value
#pairs.
# To create a dictionary, the curly braces({}) is used and 
#the key-value pairs are denoted with (key : value)

person1 = {
	"name" : "Nikolai",
	"age" : 18,
	"occupation" : "student",
	"isEnrolled" : True,
	"subjects": ["python", "SQL", "Django"]
}
print(person1)

#To get the number of key-value pairs in the dictionary, 
#The len() method can be used

print(len(person1))

#Accessing values in the dictionary

print(person1["name"])

#The Keys() nethod will return a list of all the keys in 
#the dictionary

print(person1.keys())

#the values() method will return a list of all the values 
#in the dictionary

print(person1.values())

#The items() method will just return each item in a dictionary
#as a key-value pair

print(person1.items())

#adding the key-value pairs can be done either putting a new 
#index key and assigning a value or the update() method

person1["nationality"] = "Filipino"
person1.update({"favorite food": "Sinigang"})
print(person1)

# Deleting entries can be done using the pop() method or
# the del keyword
person1.pop("favorite food")
del person1["nationality"]
print(person1)

#clear() method empties the dictionary
person2 = {
	"name" : "John",
	"age" : 25
} 
print(person2)

person2.clear()
print(person2)

#looping through dictionaries
for key in person1:
	print(f"The value of {key} is {person1[key]}")

# Nexted Dictionaries - dictionaries can be nested inside
#each other

person3 = {
	"name" : "Monika",
	"age" : 20,
	"occupation" : "writer",
	"isEnrolled" : True,
	"subjects": ["python", "SQL", "Django"]
}

classroom = {
	"student1": person1,
	"student2":person3,
}

print(classroom)

#Mini-Exercise 
#1. Create a car dictionary with the following keys
#brand,model,year of make,color
#2. print the following statements from the details
#I own a <brand> <model> and it was made in <year of make>

car = {
	"brand" : "Lamborghini",
	"Model" : "Aventador",
	"year_of_make" : 2023,
	"color" : "black",
}

print(f"I own a {car['brand']} {car['Model']}, and it was made in {car['year_of_make']}.")

#Functions
#Functions are blocks of code that runs when called 
#The "def" keyword is used to create a function. The syntax 
#is def <functionName>()

#define a function called my_greeting
def my_greeting():
	#code to be run when my_greeting is called back

	print("Hello user")

#calling/invoking a function

my_greeting()

#parameters can be added to function to have more control
#to what the inputs for the function will be

def greet_user(username):
	#prints out the value of the username paramete
	print(f'Hello, {username}!')

#Arguments are the values that are substituted to the parameters

greet_user("Bob") 
greet_user("Amy")

# From a function's perspective:
#A parameter is a variable listed inside the parentheses in
#the function definition
#Argument is the value that is sent to the function when it
#is called

#return statement - the "return" keyword functions

def addition(num1, num2):
	return num1 + num2

sum = addition(1,2)
print(f"the sum is {sum}.")

#Lambda Functions
# A lambda function is a small anonymous function that can
#be used for callbacks

greeting = lambda person : f'hello{person}'
print(greeting(" Elsie"))
print(greeting(" Anthony"))

multiply = lambda a, b : a * b 
print(multiply (5,6))
print(multiply(6,99))

#Classes 
#Classes would serve as blueprints to describe the concepts
#of objects 
#to create a class, the "class" keyword is used along with 
#the class name that starts with an uppercase character
#class ClassName():

class car():
	#properties
	def __init__(self,brand,model,year_of_make):
		self.brand = brand
		self.model = model
		self.year_of_make = year_of_make
	#other properties can be added and assigned hard-coded values
		self.fuel = "Gasoline"
		self.fuel_level = 0


	# methods
	def fill_fuel(self): 
		print(f'Current fuel level: {self.fuel_level}')
		print("filling up the fuel tank ...")
		self.fuel_level = 100
		print(f"new fuel level: {self.fuel_level}")

	def drive(self,distance):
		print(f'The car is driven {distance} Kilometers.')
		print(f'The fuel level left: {self.fuel_level - distance}')

#Creating a new instance is done by calling the class and 
#provide the arguments 

new_car = car("Nissan","GT-R","2019")

#Displaying attributes can be done using the dot notation

print(f"my car is a {new_car.brand} {new_car.model}")

#calling methods of the instance 
new_car.fill_fuel()
new_car.drive(50)